# Beta Program Portal

Application for centrailized authentication users for Beta Program

## Dependencies

- Python 3.7.x or higher
- [Pipfile](https://github.com/pypa/pipfile)

## Installation

Copy environment file `.env.xxx` to  `.env` according to your environment and add necessary values.

Initiate virtual environment and generate Pipfile and Pipfile.lock by running:

```sh
pipenv lock
```

Install dependencies and get into virtual environment.

```sh
pipenv install && pipenv shell
```

Pipenv will automatically loads environment varibles from `.env` variables,
if they exist.

## Development

### dependencies

Local or development server should import dev dependencies from Pipfile too by
this command.

```sh
pipenv install --dev && pipenv shell
```

### docker-compose

Build and run Postgresql from Docker

```sh
docker-compose up -d db
```

Run migration and create super user

```sh
python manage.py migrate
python manage.py createsuperuser
```

## Usage

### docker-compose

```sh
docker-compose up 
```

### Create super user

```sh
docker exec -it beta-program-portal-app python manage.py createsuperuser
```

## Deployment

1. Create project in GCP (in case you still don't have project)

2. Set your gcloud command to current project

```sh
gcloud config set project {PROJECT_ID}
```

3. Create GKE Cluster in GCP

4. Enable `Cloud SQL Admin API` in GCP APIs and Services

5. Build the container image

```
docker build --target prod -t gcr.io/{PROJECT_ID}/beta-program-portal:v{x}.{x.x.x} .
```

6. Push the Docker image to Container Registry

```
docker push gcr.io/${PROJECT_ID}/beta-program-portal:v{x.x.x}
```

7. Create CloudSQL then create credentials (username, password and database)

8. Create Service Account in GCP and select role of the following roles:

```
Cloud SQL > Cloud SQL Client
Cloud SQL > Cloud SQL Editor
Cloud SQL > Cloud SQL Admin
```

9. Create k8s secret for CloudSQL by

```sh
kubectl create secret generic beta-program-portal-cloud-sql \
  --from-literal=dbname={dbname} \
  --from-literal=username={username} \
  --from-literal=password={password} \
  --from-file=service-account-cloud-sql.json=./{filename from 8.}
```

10. Create static IP address by this command

```
gcloud compute addresses create {static_ip_name}
```

11. Config Helm chart variables in `k8s/Chart.yaml` and `k8s/values.yaml`

12. Deploy by this command

```
helm install k8s --generate-name
```

### Update

In case it has an update

1. Build new image and tag new version in Docker image

```
docker build --target prod -t gcr.io/{PROJECT_ID}/beta-program-portal:v{x}.{x.x.x} .
```

2. Adjust new image tag value in `k8s/values.yaml`

```
image:
  tag: "v.{x.x.x}"
```

3. Adjust `appVersion` value in `k8s/Chart.yaml` if it is a major change

4. apply change by this command

```
helm upgrade {release name} k8s
```
