############ Base ############
FROM python:3.7 as base

RUN apt-get -y update \
    && apt-get -y upgrade \
    && mkdir /app

WORKDIR /app
###############################


############ Build ############
FROM base as build

ENV PYTHONUNBUFFERED 1

COPY . /app

RUN pip install --upgrade pip \
    && pip install pipenv
###############################


######### Production ##########
FROM build as prod

RUN pipenv install --system --deploy

EXPOSE 8000

CMD ["./build.sh"]
###############################


############ Test #############
FROM build as test

RUN pipenv install --system --dev

EXPOSE 8000

CMD ["./build.sh"]
###############################
