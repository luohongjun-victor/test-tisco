#!/bin/sh

python manage.py collectstatic --no-input --clear
python manage.py wait_for_db
python manage.py migrate

gunicorn --bind=0.0.0.0:8000 --log-level=debug --timeout=90 --graceful-timeout=10 beta_program_portal.wsgi
