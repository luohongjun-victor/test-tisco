from django.apps import AppConfig


class BetaProgramConfig(AppConfig):
    name = "beta_program"
