from django.contrib import admin

from .forms.models import BetaProgramAddForm, BetaProgramChangeForm
from .models.beta_program import BetaProgram


class BetaProgramAdmin(admin.ModelAdmin):
    list_display = (
        "app_code",
        "app_name",
        "expired_date",
        "extended_amount",
        "is_active",
    )

    def get_form(self, request, obj=None, **kwargs):
        self.form = BetaProgramChangeForm if obj else BetaProgramAddForm

        return super(BetaProgramAdmin, self).get_form(request, obj, **kwargs)

    def save_model(self, request, obj, form, change):
        update_fields = []
        initial_date = form.initial["expired_date"]
        cleaned_date = form.cleaned_data["expired_date"]

        if change and obj._is_extendable(initial_date, cleaned_date):
            update_fields.append("extend")
        elif change and obj._is_expired(cleaned_date):
            update_fields.append("deactivate")

        obj.save(update_fields=update_fields)


admin.site.register(BetaProgram, BetaProgramAdmin)
