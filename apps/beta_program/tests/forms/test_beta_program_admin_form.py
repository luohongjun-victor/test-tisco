from datetime import date, timedelta

from django.test import TestCase

from apps.beta_program.forms.models import BetaProgramAddForm, BetaProgramChangeForm


class TestBetaProgramForm(TestCase):
    def setUp(self):
        self.form_data = {
            "app_code": "app001",
            "app_name": "application 001",
            "is_active": True,
        }
        self.form_data.update(expired_date=date.today() + timedelta(days=45))
        self.form_change_data = self.form_data

    def test_beta_program_add_form_valid_data(self):
        form = BetaProgramAddForm(data=self.form_data)

        self.assertTrue(form.is_valid())

    def test_beta_program_change_form_valid_data(self):
        form = BetaProgramChangeForm(data=self.form_change_data)

        self.assertTrue(form.is_valid())
