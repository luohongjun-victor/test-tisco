from datetime import date, timedelta

from django.db.utils import DataError, IntegrityError
from django.test import TestCase

from apps.beta_program.models.beta_program import BetaProgram


class TestBetaProgram(TestCase):
    def setUp(self):
        self.beta_program = BetaProgram.objects.create(
            app_code="app001",
            app_name="Application 001",
            app_desc="Application 001 Description",
        )

    def test_incorrect_model_str(self):
        self.assertNotEqual(str(self.beta_program), "app001")

    def test_correct_model_str(self):
        self.assertEqual(str(self.beta_program), "Application 001")

    def test_model_attributes(self):
        self.assertEqual(self.beta_program.app_code, "app001")
        self.assertEqual(self.beta_program.app_name, "Application 001")
        self.assertEqual(self.beta_program.app_desc, "Application 001 Description")

    def test_create_beta_propgram_with_app_code_longer_than_max_length(self):
        with self.assertRaises(DataError):
            BetaProgram.objects.create(
                app_code="app0001",
                app_name="application0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001",
            )

    def test_create_beta_propgram_with_app_name_longer_than_max_length(self):
        with self.assertRaises(DataError):
            BetaProgram.objects.create(
                app_code="app002",
                app_name="application00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001",
            )

    def test_create_beta_propgram_with_duplicate_app_code(self):
        with self.assertRaises(IntegrityError):
            BetaProgram.objects.create(app_code="app001", app_name="Application 001")

    def test_create_beta_propgram_automatically_sets_expired_date_to_next_45_days(self):
        beta_program_lifetime = date.today() + timedelta(days=45)

        self.assertEqual(beta_program_lifetime, self.beta_program.expired_date)

    def test_extend_beta_program_updates_status_and_extended_amount(self):
        update_fields = ["extend"]

        beta_program = BetaProgram.objects.create(
            app_code="app002",
            app_name="Application 002",
            app_desc="Application 002 Description",
        )

        beta_program.is_active = False
        existing_extended_amount = beta_program.extended_amount

        beta_program.save(update_fields)

        self.assertEqual(existing_extended_amount + 1, beta_program.extended_amount)
        self.assertTrue(beta_program.is_active)

    def test_deactivate_beta_program_status_sets_status_to_false(self):
        update_fields = ["deactivate"]

        beta_program = BetaProgram.objects.create(
            app_code="app003",
            app_name="Application 003",
            app_desc="Application 003 Description",
        )

        self.assertTrue(beta_program.is_active)

        beta_program.save(update_fields)

        self.assertFalse(beta_program.is_active)
