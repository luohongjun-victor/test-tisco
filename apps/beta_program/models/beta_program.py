from datetime import date, timedelta

from django.db import models


class BetaProgram(models.Model):
    LIFETIME = 45

    class Meta:
        db_table = "beta_programs"

    app_code = models.CharField(
        verbose_name="application code", max_length=6, unique=True
    )
    app_name = models.CharField(verbose_name="application name", max_length=255)
    app_desc = models.TextField(
        verbose_name="application desc", default=None, null=True, blank=True
    )
    created_at = models.DateTimeField(verbose_name="created at", auto_now_add=True)
    expired_date = models.DateField(verbose_name="expired date")
    extended_amount = models.IntegerField(verbose_name="extended amount", default=0)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.app_name

    def save(self, update_fields=None, *args, **kwargs):
        self.__hook_create_or_update(update_fields)
        super().save(*args, **kwargs)

    # protect

    def _is_extendable(self, current_date, new_date):
        return current_date < new_date

    def _is_expired(self, new_date):
        return date.today() >= new_date

    # private

    def __hook_create_or_update(self, update_fields):
        if self._state.adding:
            self.__hook_create()
        elif not self._state.adding:
            self.__hook_update(update_fields)

    def __hook_create(self):
        self.expired_date = date.today() + timedelta(days=self.LIFETIME)

    def __hook_update(self, update_fields):
        if "extend" in update_fields:
            self.extended_amount += 1
            self.is_active = True
        elif "deactivate" in update_fields:
            self.is_active = False
