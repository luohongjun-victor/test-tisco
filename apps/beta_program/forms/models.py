from django.forms import ModelForm

from apps.beta_program.models.beta_program import BetaProgram


class BetaProgramAddForm(ModelForm):
    class Meta:
        model = BetaProgram
        fields = ("app_code", "app_name", "is_active")


class BetaProgramChangeForm(ModelForm):
    class Meta:
        model = BetaProgram
        fields = ("app_code", "app_name", "app_desc", "expired_date", "is_active")
