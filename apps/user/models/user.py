from django.contrib.auth.models import AbstractBaseUser
from django.db import models

from ..manager import Manager as UserManager


class User(AbstractBaseUser):
    class Meta:
        db_table = "users"

    email = models.EmailField(
        verbose_name="email address", max_length=255, unique=True,
    )
    username = models.CharField(verbose_name="username", max_length=50, unique=True,)
    created_at = models.DateTimeField(verbose_name="created at", auto_now_add=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = ["email"]

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        return self.is_admin
