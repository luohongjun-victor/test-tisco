from django.contrib import admin

from .models.company import Company
from .models.department import Department
from .models.job_title import JobTitle
from .models.unit import Unit

admin.site.register(Company)
admin.site.register(Department)
admin.site.register(JobTitle)
admin.site.register(Unit)
