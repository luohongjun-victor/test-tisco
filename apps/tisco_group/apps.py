from django.apps import AppConfig


class TiscoGroupConfig(AppConfig):
    name = 'tisco_group'
