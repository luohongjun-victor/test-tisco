from django.db import models

from .department import Department


class Unit(models.Model):
    class Meta:
        db_table = "units"

    code = models.CharField(
        verbose_name="unit code", max_length=6, unique=True, default=None
    )
    name = models.CharField(
        verbose_name="unit name", max_length=255, unique=True, default=None
    )
    desc = models.TextField(
        verbose_name="unit desc", default=None, null=True, blank=True
    )
    created_at = models.DateTimeField(verbose_name="created at", auto_now_add=True)
    is_active = models.BooleanField(default=True)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
