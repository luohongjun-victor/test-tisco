from django.db import models


class Company(models.Model):
    class Meta:
        db_table = "companies"

    code = models.IntegerField(verbose_name="company code", primary_key=True)
    name = models.CharField(verbose_name="company name", max_length=255, unique=True,)
    desc = models.TextField(
        verbose_name="company desc", default=None, null=True, blank=True
    )
    created_at = models.DateTimeField(verbose_name="created at", auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name
