from django.db import models

from .company import Company


class Department(models.Model):
    class Meta:
        db_table = "departments"

    function_code = models.CharField(
        verbose_name="function code", max_length=6, unique=True, default=None
    )
    function_name = models.CharField(
        verbose_name="function name", max_length=255, unique=True, default=None
    )
    function_desc = models.TextField(
        verbose_name="function desc", default=None, null=True, blank=True
    )
    created_at = models.DateTimeField(verbose_name="created at", auto_now_add=True)
    is_active = models.BooleanField(default=True)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)

    def __str__(self):
        return self.function_name
