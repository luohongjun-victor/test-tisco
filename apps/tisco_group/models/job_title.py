from django.db import models


class JobTitle(models.Model):
    class Meta:
        db_table = "job_titles"

    code = models.CharField(
        verbose_name="job title code", max_length=6, unique=True, default=None
    )
    name = models.CharField(verbose_name="job title name", max_length=50)
    desc = models.TextField(verbose_name="job title desc", blank=True)
    created_at = models.DateTimeField(verbose_name="created at", auto_now_add=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name
