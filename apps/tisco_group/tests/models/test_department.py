from django.db.utils import DataError, IntegrityError
from django.test import TestCase

from apps.tisco_group.models.company import Company
from apps.tisco_group.models.department import Department


class TestDepartment(TestCase):
    def setUp(self):
        self.company = Company.objects.create(
            code=10, name="TISCO BANK", desc="TISCO Bank Public Company Limited."
        )
        self.department = Department.objects.create(
            function_code="DBI",
            function_name="Digital Banking Business & Innovation",
            company=self.company,
        )

    def test_incorrect_model_str(self):
        self.assertNotEqual(str(self.department), "Digital Banking Business")

    def test_correct_model_str(self):
        self.assertEqual(str(self.department), "Digital Banking Business & Innovation")

    def test_model_attributes(self):
        self.assertEqual(self.department.function_code, "DBI")
        self.assertEqual(
            self.department.function_name, "Digital Banking Business & Innovation"
        )

    def test_create_department_with_function_code_longer_than_max_length(self):
        with self.assertRaises(DataError):
            Department.objects.create(
                function_code="DBIDBID",
                function_name="Digital Banking Business & Innovation",
                company=self.company,
            )

    def test_create_department_with_function_name_longer_than_max_length(self):
        with self.assertRaises(DataError):
            Department.objects.create(
                function_code="DBB",
                function_name="DBIiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii",
                company=self.company,
            )

    def test_create_department_with_duplicate_function_code(self):
        with self.assertRaises(IntegrityError):
            Department.objects.create(
                function_code="DBI",
                function_name="Digital Banking Business & Innovation X",
                company=self.company,
            )

    def test_create_department_with_duplicate_function_name(self):
        with self.assertRaises(IntegrityError):
            Department.objects.create(
                function_code="DBG",
                function_name="Digital Banking Business & Innovation",
                company=self.company,
            )
