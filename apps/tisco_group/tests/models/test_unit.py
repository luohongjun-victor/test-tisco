from django.db.utils import DataError, IntegrityError
from django.test import TestCase

from apps.tisco_group.models.company import Company
from apps.tisco_group.models.department import Department
from apps.tisco_group.models.unit import Unit


class TestUnit(TestCase):
    def setUp(self):
        self.company = Company.objects.create(
            code=10, name="TISCO BANK", desc="TISCO Bank Public Company Limited."
        )
        self.department = Department.objects.create(
            function_code="DBI",
            function_name="Digital Banking Business & Innovation",
            company=self.company,
        )
        self.unit = Unit.objects.create(
            code="DA",
            name="Digital Accelerator",
            desc="Digital Accelerator",
            department=self.department,
        )

    def test_incorrect_model_str(self):
        self.assertNotEqual(str(self.unit), "Digital Accelerator X")

    def test_correct_model_str(self):
        self.assertEqual(str(self.unit), "Digital Accelerator")

    def test_model_attributes(self):
        self.assertEqual(self.unit.code, "DA")
        self.assertEqual(self.unit.name, "Digital Accelerator")
        self.assertEqual(self.unit.desc, "Digital Accelerator")

    def test_create_unit_with_code_longer_than_max_length(self):
        with self.assertRaises(DataError):
            Unit.objects.create(
                code="DADADAX", name="Digital Accelerator", department=self.department
            )

    def test_create_unit_with_name_longer_than_max_length(self):
        with self.assertRaises(DataError):
            Unit.objects.create(
                code="DAX",
                name="Digital Accelerator000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001",
                department=self.department,
            )

    def test_create_unit_with_duplicate_code(self):
        with self.assertRaises(IntegrityError):
            Unit.objects.create(
                code="DA", name="Digital Accelerator X", department=self.department
            )

    def test_create_unit_with_duplicate_name(self):
        with self.assertRaises(IntegrityError):
            Unit.objects.create(
                code="DSS", name="Digital Accelerator", department=self.department
            )
