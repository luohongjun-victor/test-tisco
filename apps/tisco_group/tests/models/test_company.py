from django.db.utils import DataError, IntegrityError
from django.test import TestCase

from apps.tisco_group.models.company import Company


class TestCompany(TestCase):
    def setUp(self):
        self.company = Company.objects.create(
            code=10,
            name="Digital Banking Business & Innovation",
            desc="Digital Banking Business & Innovation",
        )

    def test_incorrect_model_str(self):
        self.assertNotEqual(str(self.company), "DBI")

    def test_correct_model_str(self):
        self.assertEqual(str(self.company), "Digital Banking Business & Innovation")

    def test_model_attributes(self):
        self.assertEqual(self.company.code, 10)
        self.assertEqual(self.company.name, "Digital Banking Business & Innovation")
        self.assertEqual(self.company.desc, "Digital Banking Business & Innovation")

    def test_create_company_with_invalid_code_type(self):
        with self.assertRaises(ValueError):
            Company.objects.create(code="dbi", name="DBI")

    def test_create_company_with_name_longer_than_max_length(self):
        with self.assertRaises(DataError):
            Company.objects.create(
                code=11,
                name="DBIiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii",
            )

    def test_create_company_with_duplicate_code(self):
        with self.assertRaises(IntegrityError):
            Company.objects.create(code=10, name="DBI")

    def test_create_company_with_duplicate_name(self):
        with self.assertRaises(IntegrityError):
            Company.objects.create(
                code=10, name="Digital Banking Business & Innovation"
            )
