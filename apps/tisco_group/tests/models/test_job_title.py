from django.db.utils import DataError, IntegrityError
from django.test import TestCase

from apps.tisco_group.models.job_title import JobTitle


class TestJobTitle(TestCase):
    def setUp(self):
        self.jobtitle = JobTitle.objects.create(
            id=1, code="FSD", name="Full Stack Developer", desc="Full Stack Developer",
        )

    def test_incorrect_model_str(self):
        self.assertNotEqual(str(self.jobtitle), "Full Stack Developer X")

    def test_correct_model_str(self):
        self.assertEqual(str(self.jobtitle), "Full Stack Developer")

    def test_model_attributes(self):
        self.assertEqual(self.jobtitle.code, "FSD")
        self.assertEqual(self.jobtitle.name, "Full Stack Developer")
        self.assertEqual(self.jobtitle.desc, "Full Stack Developer")

    def test_create_job_title_with_code_longer_than_max_length(self):
        with self.assertRaises(DataError):
            JobTitle.objects.create(code="FSDFSDX", name="Full Stack Developer")

    def test_create_job_title_with_name_longer_than_max_length(self):
        with self.assertRaises(DataError):
            JobTitle.objects.create(
                code="FSC", name="Full Stack Developer0000000000000000000000000000001",
            )

    def test_create_job_title_with_duplicate_id(self):
        with self.assertRaises(IntegrityError):
            JobTitle.objects.create(id=1, code="FSR", name="Full Stack Developer XS")

    def test_create_job_title_with_duplicate_code(self):
        with self.assertRaises(IntegrityError):
            JobTitle.objects.create(code="FSD", name="Full Stack Developer X")
